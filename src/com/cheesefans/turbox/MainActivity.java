package com.cheesefans.turbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.*;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cheesefans.base.util.DataUtil;
import com.cheesefans.base.view.pagger.Effect;
import com.cheesefans.base.view.pagger.Item;
import com.cheesefans.base.view.pagger.Wrapper;
import com.cheesefans.turbox.alarm.AlarmActivity;
import com.cheesefans.turbox.clock.ClockActivity;
import com.cheesefans.turbox.compass.CompassActivity;
import com.cheesefans.turbox.turdo.TurdoActivity;

import java.util.Vector;

public final class MainActivity extends Activity {


    private static final int ROW = 2;
    private static final int COL = 3;
    private static final String key_row = "row";
    private static final String key_col = "col";
    private static final String key_effect = "effect";

    private Effect effect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(Menu.NONE, 0, 0, getString(R.string.set_effect)).setIcon(android.R.drawable.ic_menu_preferences);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 0:
                Effect[] effects = Effect.values();
                String[] strings = new String[effects.length];
                for (int i = 0; i < effects.length; i++) {
                    strings[i] = effects[i].toString();
                }
                new AlertDialog.Builder(this).setTitle(getString(R.string.set_effect)).setSingleChoiceItems(strings, effect.ordinal(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = DataUtil.getInstance(getApplicationContext()).getPreferences().edit();
                        editor.putInt(key_effect, which);
                        editor.commit();
                        init();
                        dialog.cancel();
                    }
                }).show();
                break;
        }
        return true;
    }

    private void init() {
        Vector<Item> items = new Vector<Item>();

        items.add(new Item(getString(R.string.clock), createItemView(this, getString(R.string.clock), getResources().getDrawable(R.drawable.clock)), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ClockActivity.class));
            }
        }, 1, 1));

        items.add(new Item(getString(R.string.alarm), createItemView(this, getString(R.string.alarm), getResources().getDrawable(R.drawable.alarm)), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AlarmActivity.class));
            }
        }, 1, 1));

        items.add(new Item(getString(R.string.compass), createItemView(this, getString(R.string.compass), getResources().getDrawable(R.drawable.compass)), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CompassActivity.class));
            }
        }, 1, 1));

        items.add(new Item(getString(R.string.turdo), createItemView(this, getString(R.string.turdo), getResources().getDrawable(R.drawable.turdo)), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TurdoActivity.class));
            }
        }, 1, 1));

        int row = DataUtil.getInstance(this).getPreferences().getInt(key_row, ROW);
        int col = DataUtil.getInstance(this).getPreferences().getInt(key_col, COL);
        int index = DataUtil.getInstance(this).getPreferences().getInt(key_effect, 0);
        effect = Effect.values()[index];
        Wrapper wrapper = new Wrapper(this, row, col, effect);
        for (Item item : items) {
            wrapper.addItem(item);
        }
        setContentView(wrapper);
    }

    View createItemView(Context context, String name, Drawable drawable) {

        RelativeLayout view = new RelativeLayout(context);

        ImageView image = new ImageView(context);
        image.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        image.setBackground(drawable);

        TextView text = new TextView(context);
        text.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        text.setText(name);
        text.setTextColor(getResources().getColor(R.color.white));
        text.setGravity(Gravity.CENTER | Gravity.BOTTOM);

        view.addView(image);
        view.addView(text);

        return view;
    }
}