package com.cheesefans.turbox.clock;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.cheesefans.turbox.R;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class ClockView extends View {

    private final Drawable dial;
    private final Timer tick;
    private final Paint paint;
    private final Path path;


    public ClockView(Context context) {
        super(context);
        dial = getResources().getDrawable(R.drawable.clock_dial);
        paint = new Paint();
        path = new Path();
        tick = new Timer();
        tick.schedule(new TimerTask() {
            @Override
            public void run() {
                postInvalidate();
            }
        }, 0, 1000);
    }

    public void destory() {
        tick.cancel();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);

        double hr = (hour * 30d + minute / 60d * 30d) / 180d * Math.PI;
        double mr = (minute + second / 60d) * 6d / 180d * Math.PI;
        double sr = second * 6d / 180d * Math.PI;
        int size = Math.min(canvas.getWidth(), canvas.getHeight());
        int hw = Math.max((int) (size / 64f), 6);
        int hh = (int) (size / 4.5f);
        int mw = Math.max((int) (size / 96f), 4);
        int mh = (int) (size / 3.5f);
        int sw = Math.max((int) (size / 192f), 2);
        int sh = (int) (size / 3f);

        dial.setBounds(0, 0, size, size);
        dial.draw(canvas);

        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);

        paint.setStrokeWidth(hw);
        paint.setColor(getResources().getColor(R.color.black));
        path.reset();
        path.moveTo(size / 2, size / 2);
        path.lineTo(size / 2 + ((float) (hh * Math.sin(hr))), size / 2 - ((float) (hh * Math.cos(hr))));
        canvas.drawPath(path, paint);
        paint.setStrokeWidth(mw);
        paint.setColor(getResources().getColor(R.color.black));
        path.reset();
        path.moveTo(size / 2, size / 2);
        path.lineTo(size / 2 + ((float) (mh * Math.sin(mr))), size / 2 - ((float) (mh * Math.cos(mr))));
        canvas.drawPath(path, paint);
        paint.setStrokeWidth(sw);
        paint.setColor(getResources().getColor(R.color.red_light));
        path.reset();
        path.moveTo(size / 2, size / 2);
        path.lineTo(size / 2 + ((float) (sh * Math.sin(sr))), size / 2 - ((float) (sh * Math.cos(sr))));
        canvas.drawPath(path, paint);
    }
}

