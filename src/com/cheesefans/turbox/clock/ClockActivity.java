package com.cheesefans.turbox.clock;

import android.app.Activity;
import android.view.Gravity;
import android.widget.RelativeLayout;
import com.cheesefans.base.util.LayoutUtil;

/**
 * User: archeese
 * Date: 14-2-4
 * Time: 下午2:38
 */
public class ClockActivity extends Activity {

    private ClockView clock;

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        clock.destory();
    }

    private void init() {
        clock = new ClockView(this);
        int size = LayoutUtil.getInstance(this).getWidth(0.95f);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(size, size);
        clock.setLayoutParams(params);

        RelativeLayout layout = new RelativeLayout(this);
        layout.setGravity(Gravity.CENTER);
        layout.addView(clock);

        this.setContentView(layout);
    }
}
