/*
 * Copyright (c) 2010-2011, The MiCode Open Source Community (www.micode.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cheesefans.turbox.compass;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cheesefans.base.util.LayoutUtil;
import com.cheesefans.turbox.R;

public class CompassActivity extends Activity implements SensorEventListener {

    private static final float MAX_ROATE_DEGREE = 1.0f;

    private SensorManager sm;
    private float current;
    private float target;
    private AccelerateInterpolator interpolator;
    private final Handler handler = new Handler();
    private boolean stop;

    private LocationManager lm;
    private String provider;

    private CompassView compass;
    private TextView location;
    private TextView direction;
    private TextView angle;

    private float[] accelerometerValues = new float[3];
    private float[] magneticFieldValues = new float[3];

    private final Runnable updater = new Runnable() {
        @Override
        public void run() {
            if (compass != null && !stop) {
                if (current != target) {

                    float to = target;
                    if (to - current > 180) {
                        to -= 360;
                    } else if (to - current < -180) {
                        to += 360;
                    }

                    float distance = to - current;
                    if (Math.abs(distance) > MAX_ROATE_DEGREE) {
                        distance = distance > 0 ? MAX_ROATE_DEGREE : (-1.0f * MAX_ROATE_DEGREE);
                    }
                    current = current + ((to - current) * interpolator.getInterpolation(Math.abs(distance) > MAX_ROATE_DEGREE ? 0.4f : 0.3f));

                    current = (current + 720) % 360;
                    compass.updateDirection(current);
                }

                updateDirection();

                handler.postDelayed(updater, 20);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initResources();
        initServices();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (provider != null) {
            updateLocation(lm.getLastKnownLocation(provider));
            lm.requestLocationUpdates(provider, 2000, 10, mLocationListener);
        } else {
            location.setText(R.string.cannot_get_location);
        }
        sm.registerListener(this, sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
        sm.registerListener(this, sm.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_FASTEST);
        stop = false;
        handler.postDelayed(updater, 20);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stop = true;
        sm.unregisterListener(this);
        if (provider != null) {
            lm.removeUpdates(mLocationListener);
        }
    }

    private void initResources() {
        current = 0.0f;
        target = 0.0f;
        interpolator = new AccelerateInterpolator();
        stop = true;

        RelativeLayout layout = new RelativeLayout(this);
        RelativeLayout.LayoutParams params;
        int size;

        compass = new CompassView(this);
        size = LayoutUtil.getInstance(this).getWidth(0.95f);
        params = new RelativeLayout.LayoutParams(size, size);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        compass.setLayoutParams(params);
        compass.setImageDrawable(getResources().getDrawable(R.drawable.compass_dial));

        angle = new TextView(this);
        size = LayoutUtil.getInstance(this).getWidth(0.05f);
        angle.setTextSize(size);
        angle.setTextColor(getResources().getColor(R.color.blue_dark));
        angle.setGravity(Gravity.CENTER);
        params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        angle.setLayoutParams(params);

        ImageView arrow = new ImageView(this);
        size = LayoutUtil.getInstance(this).getWidth(0.1f);
        params = new RelativeLayout.LayoutParams(size * 2, size);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        arrow.setLayoutParams(params);
        arrow.setBackground(getResources().getDrawable(R.drawable.compass_direction));

        direction = new TextView(this);
        size = LayoutUtil.getInstance(this).getWidth(0.1f);
        direction.setTextSize(size);
        direction.setPadding(size, size, size, size);
        direction.setTextColor(getResources().getColor(R.color.blue_dark));
        direction.setGravity(Gravity.CENTER);
        params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        direction.setLayoutParams(params);

        location = new TextView(this);
        size = LayoutUtil.getInstance(this).getWidth(0.03f);
        location.setTextSize(size);
        location.setPadding(size, size, size, size);
        location.setTextColor(getResources().getColor(R.color.blue_dark));
        location.setGravity(Gravity.CENTER);
        params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        location.setLayoutParams(params);

        layout.addView(compass);
        layout.addView(angle);
        layout.addView(arrow);
        layout.addView(direction);
        layout.addView(location);

        setContentView(layout);
    }

    private void initServices() {
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        provider = lm.getBestProvider(criteria, true);

    }

    private void updateDirection() {

        StringBuilder builder = new StringBuilder();

        float degree = ((target * -1.0f) + 720) % 360;

        if (degree > 22.5f && degree < 157.5f) {
            builder.append(getString(R.string.east));
        } else if (degree > 202.5f && degree < 337.5f) {
            builder.append(getString(R.string.west));
        }

        if (degree > 112.5f && degree < 247.5f) {
            builder.append(getString(R.string.south));
        } else if (degree < 67.5 || degree > 292.5f) {
            builder.append(getString(R.string.north));
        }

        angle.setText(" " + (int) degree + getString(R.string.degree));

        direction.setText(builder.toString());
    }

    private void updateLocation(Location location) {
        if (location == null) {
            this.location.setText(R.string.getting_location);
        } else {
            StringBuilder sb = new StringBuilder();
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            if (latitude >= 0.0f) {
                sb.append(getString(R.string.location_north, getLocationString(latitude)));
            } else {
                sb.append(getString(R.string.location_south, getLocationString(-1.0 * latitude)));
            }

            sb.append("    ");

            if (longitude >= 0.0f) {
                sb.append(getString(R.string.location_east, getLocationString(longitude)));
            } else {
                sb.append(getString(R.string.location_west, getLocationString(-1.0 * longitude)));
            }

            this.location.setText(sb.toString());
        }
    }

    private String getLocationString(double input) {
        int du = (int) input;
        int fen = (((int) ((input - du) * 3600))) / 60;
        int miao = (((int) ((input - du) * 3600))) % 60;
        return String.valueOf(du) + getString(R.string.degree) + String.valueOf(fen) + "'" + String.valueOf(miao) + "\"";
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                accelerometerValues = event.values.clone();
            case Sensor.TYPE_MAGNETIC_FIELD:
                magneticFieldValues = event.values.clone();
                break;
            default:
                break;
        }
        if (magneticFieldValues != null && accelerometerValues != null) {
            float[] values = new float[3];
            float[] R = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, null, accelerometerValues, magneticFieldValues);
            if (success) {
                SensorManager.getOrientation(R, values);
                final float direction = -(float) Math.toDegrees(values[0]);
                target = (direction + 720) % 360;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private final LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (status != LocationProvider.OUT_OF_SERVICE) {
                updateLocation(lm.getLastKnownLocation(CompassActivity.this.provider));
            } else {
                location.setText(R.string.cannot_get_location);
            }
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(Location location) {
            updateLocation(location);
        }

    };
}
