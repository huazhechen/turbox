/*
 * Copyright (c) 2010-2011, The MiCode Open Source Community (www.micode.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cheesefans.turbox.compass;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class CompassView extends ImageView {

    private float direction;
    private Drawable compass;

    public CompassView(Context context) {
        super(context);
        this.direction = 0f;
        this.compass = null;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        compass = getDrawable();
        compass.setBounds(0, 0, getWidth(), getHeight());
        canvas.save();
        canvas.rotate(direction, getWidth() / 2, getHeight() / 2);
        compass.draw(canvas);
        canvas.restore();
    }

    public void updateDirection(float direction) {
        this.direction = direction;
        invalidate();
    }
}
