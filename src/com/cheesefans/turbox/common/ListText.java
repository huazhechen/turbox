package com.cheesefans.turbox.common;

import android.content.Context;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cheesefans.base.util.LayoutUtil;

/**
 * 大小不会变化的文本
 *
 * @author hua
 */
public class ListText extends TextView {

    public ListText(final Context context) {
        super(context);
        /**
         * 宽度为满屏幕，高度为0.08屏幕高
         */
        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutUtil.getInstance(context)
                .getHeight(0.08f));
        setLayoutParams(params);
        /**
         * 左侧内边距为0.03屏幕宽
         */
        setPadding(LayoutUtil.getInstance(context).getWidth(0.03f), 0,
                0, 0);
        /**
         * 输入框为单行模式
         */
        setSingleLine();
        /**
         * 文本居中
         */
        setGravity(Gravity.CENTER_VERTICAL);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, int heightMeasureSpec) {
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(
                getLayoutParams().height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
