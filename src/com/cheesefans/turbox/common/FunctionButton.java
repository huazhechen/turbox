package com.cheesefans.turbox.common;

import android.content.Context;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.cheesefans.base.util.LayoutUtil;

/**
 * 大小不会变化的按钮
 *
 * @author hua
 */
public class FunctionButton extends Button {

    public FunctionButton(final Context context) {
        super(context);
        /**
         * 大小为0.08屏幕高度
         */
        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                LayoutUtil.getInstance(context).getHeight(0.08f), LayoutUtil
                .getInstance(context).getHeight(0.08f));
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        setLayoutParams(params);
        setFocusable(false);
        setClickable(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(getLayoutParams().width,
                MeasureSpec.EXACTLY);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(
                getLayoutParams().height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
