package com.cheesefans.turbox.turdo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import com.cheesefans.base.util.DataUtil;
import com.cheesefans.base.util.LayoutUtil;
import com.cheesefans.base.view.list.DragListView;
import com.cheesefans.base.view.list.DragListView.DropListener;
import com.cheesefans.base.view.list.ListViewAnimationAdapter;
import com.cheesefans.base.view.list.Rotate3dAnimation;
import com.cheesefans.turbox.turdo.task.AddTaskButton;
import com.cheesefans.turbox.turdo.task.Task;
import com.cheesefans.turbox.turdo.task.TaskView;
import org.json.JSONArray;
import org.json.JSONException;

public class TurdoActivity extends Activity {

    private Context context;
    private static final int ADDID = 1;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        /**
         * 保存上下文
         */
        context = this;
        /**
         * 主布局
         */
        final RelativeLayout main = new RelativeLayout(this);
        /**
         * 背景色白色
         */
        main.setBackgroundColor(Color.WHITE);
        /**
         * 大小为全屏
         */
        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        main.setLayoutParams(params);
        /**
         * 居中显示
         */
        main.setGravity(Gravity.CENTER);

        /**
         * 可拖拽列表
         */
        final DragListView list = new DragListView(this);
        /**
         * 大小设置
         */
        final RelativeLayout.LayoutParams viewParams = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutUtil.getInstance(this)
                .getHeight(0.8f));
        /**
         * 在点击按钮下方
         */
        viewParams.addRule(RelativeLayout.BELOW, ADDID);
        list.setLayoutParams(viewParams);

        /**
         * 适配器
         */
        final ListViewAnimationAdapter adapter = new ListViewAnimationAdapter(list) {
            @Override
            public View getView(final int position, final View convertView,
                                final ViewGroup parent) {
                return new TaskView(context, this, position);
            }

            @Override
            public void read(Context context) {
                final SharedPreferences preferences = DataUtil.getInstance(
                        context).getPreferences();
                final String data = preferences.getString("tasks", "");
                try {
                    final JSONArray array = new JSONArray(data);
                    for (int i = 0; i < array.length(); i++) {
                        datas.add(new Task(array.getJSONObject(i)));
                    }
                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void write(Context context) {
                final SharedPreferences preferences = DataUtil.getInstance(
                        context).getPreferences();
                final Editor editor = preferences.edit();
                final JSONArray array = new JSONArray();
                for (Object data : datas) {
                    array.put(((Task) data).toJson());
                }
                editor.putString("tasks", array.toString());
                editor.commit();

            }

        };
        /**
         * 增加数据
         */
        adapter.read(this);
        /**
         * 通知数据就绪
         */
        adapter.notifyDataSetInvalidated();
        /**
         * 设置按键监听
         */
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                if (position >= 0 && position < adapter.getCount()) {
                    /**
                     * 位置正确则反转
                     */
                    final Task task = (Task) adapter.getItem(position);
                    /**
                     * 翻转到垂直
                     */
                    final Rotate3dAnimation animation = new Rotate3dAnimation(
                            0, 90);
                    animation
                            .setAnimationListener(new AnimationListener() {

                                @Override
                                public void onAnimationStart(
                                        final Animation animation) {
                                }

                                @Override
                                public void onAnimationRepeat(
                                        final Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(
                                        final Animation animation) {
                                    /**
                                     * 翻转结束时从垂直反转到正常
                                     */
                                    final Rotate3dAnimation back = new Rotate3dAnimation(
                                            -90, 0);
                                    task.setComplete(!task.isComplete());
                                    adapter.modify(context, position,
                                            task);
                                    ((TaskView) view).update(task);
                                    back.setAnimationListener(new AnimationListener() {
                                        @Override
                                        public void onAnimationStart(
                                                final Animation animation) {
                                        }

                                        @Override
                                        public void onAnimationRepeat(
                                                final Animation animation) {
                                        }

                                        @Override
                                        public void onAnimationEnd(
                                                final Animation animation) {
                                        }
                                    });
                                    view.startAnimation(back);
                                }
                            });
                    view.startAnimation(animation);
                }
            }
        });
        /**
         * 拖拽结束时，交换位置
         */
        list.setDropListener(new DropListener() {
            @Override
            public void drop(final int from, final int to) {
                TurdoActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.move(from, to);
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        /**
         * 增加按钮
         */
        final AddTaskButton add = new AddTaskButton(this, adapter);
        add.setId(ADDID);

        main.addView(add);
        main.addView(list);
        this.setContentView(main);
    }
}
