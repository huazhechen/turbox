package com.cheesefans.turbox.turdo.task;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import com.cheesefans.base.util.LayoutUtil;
import com.cheesefans.base.view.list.ListViewAnimationAdapter;
import com.cheesefans.turbox.R;
import com.cheesefans.turbox.common.FunctionButton;
import com.cheesefans.turbox.common.ListText;

/**
 * 任务视图
 *
 * @author hua
 */
public class TaskView extends RelativeLayout {

    /**
     * 内容文本
     */
    private ListText content;
    /**
     * 删除按钮
     */
    private FunctionButton button;

    /**
     * 私有构造函数
     *
     * @param context 上下文
     */
    private TaskView(final Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context  上下文
     * @param adapter  带动画的适配器
     * @param position 位置
     */
    public TaskView(final Context context, final ListViewAnimationAdapter adapter,
                    final int position) {
        this(context);
        /**
         * 宽度为满屏幕，高度为0.12屏幕高
         */
        final AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT, LayoutUtil
                .getInstance(context).getHeight(0.12f));
        setLayoutParams(params);
        /**
         * 左右两边内边距0.1屏幕宽，上下0.02f的空白
         */
        setPadding(LayoutUtil.getInstance(context).getWidth(0.1f), LayoutUtil
                .getInstance(context).getHeight(0.02f),
                LayoutUtil.getInstance(context).getWidth(0.1f), LayoutUtil
                .getInstance(context).getHeight(0.02f));
        /**
         * 内容文本框
         */
        content = new ListText(context);
        /**
         * 文本大小为0.025屏幕宽
         */
        content.setTextSize(LayoutUtil.getInstance(context).getWidth(0.025f));

        /**
         * 删除按钮
         */
        button = new FunctionButton(context);
        /**
         * 背景为删除按钮
         */
        button.setBackgroundResource(R.drawable.delete);
        /**
         * 增加按钮按下监听
         */
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                new AlertDialog.Builder(context)
                        .setTitle(context.getString(R.string.delete_task))
                        .setMessage(context.getString(R.string.delete_task_confirm))
                        .setPositiveButton(context.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface d, int w) {
                                        adapter.remove(context, position);
                                    }
                                })
                        .setNegativeButton(context.getString(R.string.cancel), null)
                        .show();
            }
        });

        this.addView(content);
        this.addView(button);

        /**
         * 更新数据
         */
        final Task task = (Task) adapter.getItem(position);
        update(task);
    }

    public void update(final Task task) {
        if (task.isComplete()) {
            /**
             * 完成的Task
             */
            content.setBackgroundColor(Color.GRAY);
            content.setTextColor(Color.LTGRAY);
            content.getPaint().setFlags(
                    Paint.DITHER_FLAG | Paint.STRIKE_THRU_TEXT_FLAG);
            button.setVisibility(View.VISIBLE);
            button.setClickable(true);
        } else {
            /**
             * 未完成的Task
             */
            content.setBackgroundColor(getResources().getColor(R.color.blue_light));
            content.setTextColor(getResources().getColor(R.color.white));
            content.getPaint().setFlags(Paint.DITHER_FLAG);
            button.setVisibility(View.INVISIBLE);
            button.setClickable(false);
        }
        content.setText(task.getContent());
    }

}
