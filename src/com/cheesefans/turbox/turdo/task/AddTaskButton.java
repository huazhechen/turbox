package com.cheesefans.turbox.turdo.task;

import android.content.Context;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import com.cheesefans.base.util.DisplayUtil;
import com.cheesefans.base.util.LayoutUtil;
import com.cheesefans.base.view.list.ListViewAnimationAdapter;
import com.cheesefans.turbox.R;

/**
 * 新增任务界面
 *
 * @author hua
 */
public class AddTaskButton extends RelativeLayout {

    private boolean beyond;

    /**
     * 私有构造函数
     *
     * @param context 上下文
     */
    private AddTaskButton(final Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param adapter 将要添加进入的适配器
     */
    public AddTaskButton(final Context context, final ListViewAnimationAdapter adapter) {
        this(context);
        /**
         * 宽度为满屏幕，高度为0.12屏幕高
         */
        final AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT, LayoutUtil
                .getInstance(context).getHeight(0.12f));
        setLayoutParams(params);
        /**
         * 左右两边内边距0.1屏幕宽，上下0.02f的空白
         */
        setPadding(LayoutUtil.getInstance(context).getWidth(0.1f), LayoutUtil
                .getInstance(context).getHeight(0.02f),
                LayoutUtil.getInstance(context).getWidth(0.1f), LayoutUtil
                .getInstance(context).getHeight(0.02f));
        /**
         * 输入框
         */
        final EditText input = new EditText(context);
        /**
         * 默认超长为否
         */
        beyond = false;
        /**
         * 大小为充满父控件
         */
        final LayoutParams inputParams = new LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(inputParams);
        /**
         * 文本大小为0.025屏幕宽
         */
        input.setTextSize(LayoutUtil.getInstance(context).getWidth(0.025f));
        /**
         * 输入框文本颜色为白色
         */
        input.setTextColor(getResources().getColor(R.color.white));
        /**
         * 输入框背景色为紫色
         */
        input.setBackgroundColor(getResources().getColor(R.color.purple_light));
        /**
         * 输入框为单行模式
         */
        input.setSingleLine();
        /**
         * 文本居中
         */
        input.setGravity(Gravity.CENTER_VERTICAL);
        /**
         * 输入框文本变化监听
         */
        input.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence s, final int start,
                                      final int before, final int count) {
            }

            @Override
            public void beforeTextChanged(final CharSequence s,
                                          final int start, final int count, final int after) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                /**
                 * 获取文本
                 */
                final String string = s.toString();
                /**
                 * 获取画笔
                 */
                final TextPaint paint = input.getPaint();
                /**
                 * 根据画笔计算文本长度
                 */
                final float length = paint.measureText(string);
                /**
                 * 长度大于0.65屏幕宽，则减去最后一个字符
                 */
                if (length > LayoutUtil.getInstance(context).getWidth(0.64f)) {
                    beyond = true;
                    s.delete(string.length() - 1, string.length());
                } else if (beyond) {
                    DisplayUtil.toast(context, AddTaskButton.this.getResources()
                            .getString(R.string.length_alert));
                    beyond = false;
                }
            }
        });

        /**
         * 增加按钮
         */
        final Button add = new Button(context);
        /**
         * 增加按钮大小为充满控件
         */
        final LayoutParams addParams = new LayoutParams(
                LayoutUtil.getInstance(context).getHeight(0.08f), LayoutUtil
                .getInstance(context).getHeight(0.08f));
        /**
         * 按钮在右侧
         */
        addParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        add.setLayoutParams(addParams);
        /**
         * 背景为增加按钮
         */
        add.setBackgroundResource(R.drawable.add);
        /**
         * 增加按钮按下监听
         */
        add.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                /**
                 * 获取软键盘管理器
                 */
                final InputMethodManager manager = (InputMethodManager) AddTaskButton.this
                        .getContext().getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                /**
                 * 文本不为空
                 */
                if (input.length() > 0) {
                    /**
                     * 新增Task
                     */
                    adapter.add(context, new Task(input.getEditableText()
                            .toString()));
                    /**
                     * 清空输入框
                     */
                    input.getEditableText().clear();
                    /**
                     * 收回软键盘
                     */
                    manager.hideSoftInputFromWindow(input.getWindowToken(), 0);
                } else {
                    /**
                     * 打开软键盘
                     */
                    manager.showSoftInput(input, 0);
                }
            }
        });

        this.addView(input);
        this.addView(add);

    }
}
