package com.cheesefans.turbox.turdo.task;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 任务
 *
 * @author hua
 */
public class Task {

    /**
     * 内容
     */
    private String content;
    /**
     * 是否已经完成
     */
    private boolean complete;

    /**
     * 构造函数
     *
     * @param content 内容
     */
    public Task(final String content) {
        this.content = content;
    }

    /**
     * 由JSON生成
     *
     * @param json JSON对象
     */
    public Task(final JSONObject json) {
        try {
            content = json.getString("content");
            complete = json.getBoolean("turdo");
        } catch (final JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 生成JSON对象
     *
     * @return JSON对象
     */
    public JSONObject toJson() {
        final JSONObject json = new JSONObject();
        try {
            json.put("content", content);
            json.put("turdo", complete);
        } catch (final JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public synchronized String getContent() {
        return content;
    }

    public synchronized void setComplete(final boolean complete) {
        this.complete = complete;
    }

    public synchronized boolean isComplete() {
        return complete;
    }

}
