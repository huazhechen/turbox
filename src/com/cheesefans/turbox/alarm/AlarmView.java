package com.cheesefans.turbox.alarm;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import com.cheesefans.base.util.LayoutUtil;
import com.cheesefans.base.view.list.ListViewAnimationAdapter;
import com.cheesefans.turbox.R;
import com.cheesefans.turbox.common.FunctionButton;
import com.cheesefans.turbox.common.ListText;

import java.util.Calendar;

/**
 * 任务视图
 *
 * @author hua
 */
public class AlarmView extends RelativeLayout {

    /**
     * 上下文
     */
    private Context context;
    /**
     * 适配器
     */
    private ListViewAnimationAdapter adapter;
    /**
     * 位置
     */
    private int position;
    /**
     * 内容文本
     */
    private ListText time;
    /**
     * 删除按钮
     */
    private FunctionButton button;

    /**
     * 私有构造函数
     *
     * @param context 上下文
     */
    private AlarmView(final Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context  上下文
     * @param adapter  带动画的适配器
     * @param position 位置
     */
    public AlarmView(final Context context, final ListViewAnimationAdapter adapter,
                     final int position) {
        this(context);
        this.context = context;
        this.adapter = adapter;
        this.position = position;
        /**
         * 宽度为满屏幕，高度为0.12屏幕高
         */
        final AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT, LayoutUtil
                .getInstance(context).getHeight(0.12f));
        setLayoutParams(params);
        /**
         * 左右两边内边距0.1屏幕宽，上下0.02f的空白
         */
        setPadding(LayoutUtil.getInstance(context).getWidth(0.1f), LayoutUtil
                .getInstance(context).getHeight(0.02f),
                LayoutUtil.getInstance(context).getWidth(0.1f), LayoutUtil
                .getInstance(context).getHeight(0.02f));
        /**
         * 内容文本框
         */
        time = new ListText(context);
        /**
         * 文本大小为0.025屏幕宽
         */
        time.setTextSize(LayoutUtil.getInstance(context).getWidth(0.025f));

        /**
         * 删除按钮
         */
        button = new FunctionButton(context);

        this.addView(time);
        this.addView(button);

        /**
         * 更新数据
         */
        final Alarm alarm = (Alarm) adapter.getItem(position);
        update(alarm);
    }

    public void update(final Alarm alarm) {
        if (alarm.enabled) {
            /**
             * 启动的闹钟
             */
            time.setBackgroundColor(getResources().getColor(R.color.blue_light));
            time.setTextColor(getResources().getColor(R.color.white));
            time.getPaint().setFlags(Paint.DITHER_FLAG);
            button.setBackgroundResource(R.drawable.modify);
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(final View v) {
                    Intent intent = new Intent(context, AlarmSettingActivity.class);
                    intent.putExtra(Alarms.ALARM_ID, alarm.id);
                    context.startActivity(intent);
                }
            });
        } else {
            /**
             * 未启动的闹钟
             */
            time.setBackgroundColor(Color.GRAY);
            time.setTextColor(Color.LTGRAY);
            time.getPaint().setFlags(Paint.DITHER_FLAG | Paint.STRIKE_THRU_TEXT_FLAG);
            button.setBackgroundResource(R.drawable.delete);
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(final View v) {
                    new AlertDialog.Builder(context)
                            .setTitle(context.getString(R.string.delete_alarm))
                            .setMessage(context.getString(R.string.delete_alarm_confirm))
                            .setPositiveButton(context.getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface d, int w) {
                                            adapter.remove(context, position);
                                            Alarms.deleteAlarm(context, alarm.id);
                                        }
                                    })
                            .setNegativeButton(context.getString(R.string.cancel), null)
                            .show();
                }
            });
        }
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, alarm.hour);
        c.set(Calendar.MINUTE, alarm.minutes);
        String string = DateFormat.format(Alarms.M24, c) + " ";
        int length = string.length();
        string = string + alarm.daysOfWeek.toString(context, false);
        SpannableString msp = new SpannableString(string);

        msp.setSpan(new AbsoluteSizeSpan((int) time.getTextSize() / 2), length, string.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        time.setText(msp);
    }
}
