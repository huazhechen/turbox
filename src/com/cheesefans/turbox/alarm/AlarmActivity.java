package com.cheesefans.turbox.alarm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.*;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;
import com.cheesefans.base.util.LayoutUtil;
import com.cheesefans.base.view.list.ArchListView;
import com.cheesefans.base.view.list.ListViewAnimationAdapter;
import com.cheesefans.base.view.list.Rotate3dAnimation;
import com.cheesefans.turbox.R;

public class AlarmActivity extends Activity {

    static final String PREFERENCES = "AlarmClock";
    private static final int ADDID = 1;

    private Context context;
    private ListViewAnimationAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(Menu.NONE, 0, 0, getString(R.string.settings)).setIcon(android.R.drawable.ic_menu_preferences);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 0:
                startActivity(new Intent(this, AlarmsSettingActivity.class));
                break;
        }
        return true;
    }

    private void init() {
        /**
         * 保存上下文
         */
        context = this;
        /**
         * 主布局
         */
        final RelativeLayout main = new RelativeLayout(this);
        /**
         * 背景色白色
         */
        main.setBackgroundColor(Color.WHITE);
        /**
         * 大小为全屏
         */
        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        main.setLayoutParams(params);
        /**
         * 居中显示
         */
        main.setGravity(Gravity.CENTER);

        /**
         * 可拖拽列表
         */
        final ArchListView list = new ArchListView(this);
        /**
         * 大小设置
         */
        final RelativeLayout.LayoutParams viewParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, LayoutUtil.getInstance(this)
                .getHeight(0.8f));
        /**
         * 在点击按钮下方
         */
        viewParams.addRule(RelativeLayout.BELOW, ADDID);
        list.setLayoutParams(viewParams);

        /**
         * 适配器
         */
        adapter = new ListViewAnimationAdapter(list) {
            @Override
            public View getView(final int position, final View convertView,
                                final ViewGroup parent) {
                return new AlarmView(context, this, position);
            }

            @Override
            public void read(Context context) {
                datas.clear();
                Cursor cursor = Alarms.getAlarmsCursor(getContentResolver());
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    datas.add(new Alarm(cursor));
                }
            }

            @Override
            public void write(Context context) {
            }

        };
        /**
         * 增加数据
         */
        adapter.read(this);
        /**
         * 通知数据就绪
         */
        adapter.notifyDataSetInvalidated();
        /**
         * 设置按键监听
         */
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {

                AlarmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (position >= 0 && position < adapter.getCount()) {
                            /**
                             * 位置正确则反转
                             */
                            final Alarm alarm = (Alarm) adapter.getItem(position);
                            final AlarmView view = (AlarmView) list.getChildAt(position - list.getFirstVisiblePosition());
                            if (view != null) {
                                /**
                                 * 翻转到垂直
                                 */
                                final Rotate3dAnimation animation = new Rotate3dAnimation(
                                        0, 90);
                                animation
                                        .setAnimationListener(new Animation.AnimationListener() {

                                            @Override
                                            public void onAnimationStart(
                                                    final Animation animation) {
                                            }

                                            @Override
                                            public void onAnimationRepeat(
                                                    final Animation animation) {
                                            }

                                            @Override
                                            public void onAnimationEnd(
                                                    final Animation animation) {
                                                /**
                                                 * 翻转结束时从垂直反转到正常
                                                 */
                                                final Rotate3dAnimation back = new Rotate3dAnimation(
                                                        -90, 0);
                                                alarm.enabled = !alarm.enabled;
                                                Alarms.enableAlarm(context, alarm.id, alarm.enabled);
                                                adapter.modify(context, position, alarm);
                                                view.update(alarm);
                                                back.setAnimationListener(new Animation.AnimationListener() {
                                                    @Override
                                                    public void onAnimationStart(
                                                            final Animation animation) {
                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(
                                                            final Animation animation) {
                                                    }

                                                    @Override
                                                    public void onAnimationEnd(
                                                            final Animation animation) {
                                                        view.requestLayout();
                                                    }
                                                });
                                                view.startAnimation(back);
                                            }
                                        });
                                view.startAnimation(animation);
                            }
                        }
                    }
                });
            }
        });
        /**
         * 增加按钮
         */
        final AlarmAddButton add = new AlarmAddButton(this);
        add.setId(ADDID);

        main.addView(add);
        main.addView(list);
        this.setContentView(main);
    }
}