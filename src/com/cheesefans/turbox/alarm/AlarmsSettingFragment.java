/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cheesefans.turbox.alarm;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.*;
import android.provider.Settings;
import com.cheesefans.turbox.R;

public class AlarmsSettingFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    private Context context;
    private CheckBoxPreference silent;
    private ListPreference snooze;
    private static final int ALARM_STREAM_TYPE_BIT = 1 << AudioManager.STREAM_ALARM;
    static final String KEY_ALARM_SNOOZE = "snooze";

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        context = getActivity();
        PreferenceManager pm = getPreferenceManager();
        if (context != null && pm != null) {
            PreferenceScreen screen = pm.createPreferenceScreen(context);
            if (screen != null) {

                silent = new CheckBoxPreference(context);
                silent.setTitle(context.getString(R.string.alarm_in_silent_mode_title));
                silent.setSummary(getString(R.string.alarm_in_silent_mode_summary));
                screen.addPreference(silent);

                snooze = new ListPreference(context);
                snooze.setKey(KEY_ALARM_SNOOZE);
                snooze.setTitle(context.getString(R.string.snooze_duration_title));
                snooze.setDefaultValue(getResources().getStringArray(R.array.snooze_duration_values)[1]);
                snooze.setEntries(getResources().getStringArray(R.array.snooze_duration_entries));
                snooze.setEntryValues(getResources().getStringArray(R.array.snooze_duration_values));
                screen.addPreference(snooze);

                final int types = Settings.System.getInt(context.getContentResolver(), Settings.System.MODE_RINGER_STREAMS_AFFECTED, 0);
                silent.setChecked((types & ALARM_STREAM_TYPE_BIT) == 0);
                snooze.setSummary(snooze.getEntry());
                snooze.setOnPreferenceChangeListener(this);

                setPreferenceScreen(screen);

            }
        }
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference.equals(silent)) {
            int types = Settings.System.getInt(context.getContentResolver(), Settings.System.MODE_RINGER_STREAMS_AFFECTED, 0);
            if (silent.isChecked()) {
                types &= ~ALARM_STREAM_TYPE_BIT;
            } else {
                types |= ALARM_STREAM_TYPE_BIT;
            }
            Settings.System.putInt(context.getContentResolver(), Settings.System.MODE_RINGER_STREAMS_AFFECTED, types);
            return true;
        }

        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference pref, Object newValue) {
        final int index = snooze.findIndexOfValue((String) newValue);
        snooze.setSummary(snooze.getEntries()[index]);
        return true;
    }
}
