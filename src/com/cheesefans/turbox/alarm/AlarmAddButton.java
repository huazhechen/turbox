package com.cheesefans.turbox.alarm;

import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cheesefans.base.util.LayoutUtil;
import com.cheesefans.turbox.R;

/**
 * 新增任务界面
 *
 * @author hua
 */
public class AlarmAddButton extends RelativeLayout {

    /**
     * 私有构造函数
     *
     * @param context 上下文
     */
    public AlarmAddButton(final Context context) {
        super(context);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, AlarmSettingActivity.class));
            }
        });
        /**
         * 宽度为满屏幕，高度为0.12屏幕高
         */
        final AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT, LayoutUtil
                .getInstance(context).getHeight(0.12f));
        setLayoutParams(params);
        /**
         * 左右两边内边距0.1屏幕宽，上下0.02f的空白
         */
        setPadding(LayoutUtil.getInstance(context).getWidth(0.1f), LayoutUtil
                .getInstance(context).getHeight(0.02f),
                LayoutUtil.getInstance(context).getWidth(0.1f), LayoutUtil
                .getInstance(context).getHeight(0.02f));
        /**
         * 输入框
         */
        final TextView text = new TextView(context);
        text.setText(context.getString(R.string.add_alarm));
        /**
         * 大小为充满父控件
         */
        final LayoutParams inputParams = new LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        text.setLayoutParams(inputParams);
        /**
         * 文本大小为0.025屏幕宽
         */
        text.setTextSize(LayoutUtil.getInstance(context).getWidth(0.025f));
        text.setPadding(LayoutUtil.getInstance(context).getWidth(0.025f), 0, 0, 0);
        /**
         * 输入框文本颜色为白色
         */
        text.setTextColor(getResources().getColor(R.color.white));
        /**
         * 输入框背景色为紫色
         */
        text.setBackgroundColor(getResources().getColor(R.color.purple_light));
        /**
         * 输入框为单行模式
         */
        text.setSingleLine();
        /**
         * 文本居中
         */
        text.setGravity(Gravity.CENTER_VERTICAL);

        /**
         * 增加按钮
         */
        final ImageView add = new ImageView(context);
        /**
         * 增加按钮大小为充满控件
         */
        final LayoutParams addParams = new LayoutParams(
                LayoutUtil.getInstance(context).getHeight(0.08f), LayoutUtil
                .getInstance(context).getHeight(0.08f));
        /**
         * 按钮在右侧
         */
        addParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        add.setLayoutParams(addParams);
        /**
         * 背景为增加按钮
         */
        add.setBackgroundResource(R.drawable.add);

        this.addView(text);
        this.addView(add);

    }
}
