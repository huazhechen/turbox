/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cheesefans.turbox.alarm;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.*;
import android.text.format.DateFormat;
import android.widget.TimePicker;
import android.widget.Toast;
import com.cheesefans.turbox.R;

/**
 * 管理每一个闹钟
 * 每一个闹钟对应的信息都绑定在Preference中了
 */
public class AlarmSettingFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    private Context context;

    private CheckBoxPreference enable;
    private Preference time;
    private AlarmRepeatPreference repeat;
    private AlarmRingPreference ring;
    private CheckBoxPreference vibrate;

    private int id;
    private int mHour;
    private int mMinutes;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        context = getActivity();
        PreferenceManager pm = getPreferenceManager();
        if (context != null && pm != null) {
            PreferenceScreen screen = pm.createPreferenceScreen(context);
            if (screen != null) {

                enable = new CheckBoxPreference(context);
                enable.setTitle(context.getString(R.string.enable_alarm));
                enable.setPersistent(false);
                screen.addPreference(enable);

                time = new Preference(context);
                time.setTitle(context.getString(R.string.time));
                time.setPersistent(false);
                screen.addPreference(time);

                repeat = new AlarmRepeatPreference(context);
                repeat.setTitle(context.getString(R.string.alarm_repeat));
                repeat.setPersistent(false);
                repeat.setOnPreferenceChangeListener(this);
                screen.addPreference(repeat);

                ring = new AlarmRingPreference(context);
                ring.setTitle(context.getString(R.string.alert));
                ring.setPersistent(false);
                ring.setRingtoneType(RingtoneManager.TYPE_ALARM);
                ring.setShowDefault(false);
                ring.setShowSilent(true);
                ring.setOnPreferenceChangeListener(this);
                screen.addPreference(ring);

                vibrate = new CheckBoxPreference(context);
                vibrate.setTitle(context.getString(R.string.alarm_vibrate));
                vibrate.setOnPreferenceChangeListener(this);
                vibrate.setPersistent(false);
                screen.addPreference(vibrate);

                enable.setOnPreferenceChangeListener(
                        new Preference.OnPreferenceChangeListener() {
                            public boolean onPreferenceChange(Preference p, Object newValue) {
                                enable.setChecked((Boolean) newValue);
                                if (enable.isChecked()) {
                                    popAlarmSetToast(context, mHour, mMinutes, repeat.getDaysOfWeek());
                                }
                                saveAlarm();
                                return true;
                            }
                        });


                bundle = getArguments();
                if (bundle != null) {
                    id = bundle.getInt(Alarms.ALARM_ID);
                } else {
                    id = -1;
                }
                Alarm alarm;
                if (id == -1) {
                    alarm = new Alarm();
                } else {
                    alarm = Alarms.getAlarm(context.getContentResolver(), id);
                    if (alarm == null) {
                        ((Activity) context).finish();
                        return;
                    }
                }
                updatePrefs(alarm);
                setPreferenceScreen(screen);
                if (id == -1) {
                    showTimePicker();
                }
            }
        }
    }

    private static final Handler HANDLER = new Handler();

    public boolean onPreferenceChange(final Preference p, Object newValue) {
        HANDLER.post(new Runnable() {
            public void run() {
                saveAlarm();
            }
        });
        return true;
    }

    private void updatePrefs(Alarm alarm) {
        id = alarm.id;
        enable.setChecked(alarm.enabled);
        mHour = alarm.hour;
        mMinutes = alarm.minutes;
        repeat.setDaysOfWeek(alarm.daysOfWeek);
        vibrate.setChecked(alarm.vibrate);
        ring.setAlert(alarm.alert);
        updateTime();
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
                                         Preference preference) {
        if (preference == time) {
            showTimePicker();
        }

        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    private void showTimePicker() {
        new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                mHour = hourOfDay;
                mMinutes = minute;
                updateTime();
                enable.setChecked(true);
                popAlarmSetToast(context, saveAlarm());
            }
        }, mHour, mMinutes, DateFormat.is24HourFormat(context)) {
            @Override
            public void onBackPressed() {
                super.onBackPressed();
                if (id == -1) {
                    ((Activity) context).finish();
                }
            }

            @Override
            public void show() {
                this.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), this);
                this.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (id == -1) {
                            ((Activity) context).finish();
                        }
                    }
                });
                super.show();
            }

            @Override
            protected void onStop() {
            }
        }.show();
    }


    private void updateTime() {
        time.setSummary(Alarms.formatTime(context, mHour, mMinutes,
                repeat.getDaysOfWeek()));
    }

    private long saveAlarm() {
        Alarm alarm = new Alarm();
        alarm.id = id;
        alarm.enabled = enable.isChecked();
        alarm.hour = mHour;
        alarm.minutes = mMinutes;
        alarm.daysOfWeek = repeat.getDaysOfWeek();
        alarm.vibrate = vibrate.isChecked();
        alarm.alert = ring.getAlert();

        long time;
        if (alarm.id == -1) {
            time = Alarms.addAlarm(context, alarm);
            id = alarm.id;
        } else {
            time = Alarms.setAlarm(context, alarm);
        }
        return time;
    }

    private static void popAlarmSetToast(Context context, int hour, int minute,
                                         Alarm.DaysOfWeek daysOfWeek) {
        popAlarmSetToast(context,
                Alarms.calculateAlarm(hour, minute, daysOfWeek)
                        .getTimeInMillis());
    }

    static void popAlarmSetToast(Context context, long timeInMillis) {
        String toastText = formatToast(context, timeInMillis);
        Toast toast = Toast.makeText(context, toastText, Toast.LENGTH_LONG);
        toast.show();
    }

    private static String formatToast(Context context, long timeInMillis) {
        long delta = timeInMillis - System.currentTimeMillis();
        long hours = delta / (1000 * 60 * 60);
        long minutes = delta / (1000 * 60) % 60;
        if (minutes == 0) {
            minutes = 1;
        }
        long days = hours / 24;
        hours = hours % 24;

        String day = (days == 0) ? "" : days + context.getString(R.string.day);
        String hour = (hours == 0) ? "" : hours + context.getString(R.string.hour);
        String min = minutes + context.getString(R.string.minute);
        return "已将该闹钟设置为从现在起" + day + hour + min + "后提醒。";
    }
}
